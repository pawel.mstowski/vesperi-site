# Repozytorium z wszystkimi plikami dla stronki Vesperi Design

Technologie użyte:
- https://getbootstrap.com/
- https://parceljs.org/

Przydatne narzędzia i inspiracje:
- https://imagecolorpicker.com/
- https://www.tinyletter.com/
- https://business.tutsplus.com/articles/great-landing-page-design--cms-31924
- https://unbounce.com/landing-page-examples/best-landing-page-examples/
- https://pl.pinterest.com/kabbed/landing-pages/
- https://pl.pinterest.com/ewighost/best-landing-page-design/
- https://www.toptal.com/designers/colourcode/monochrome-dark-color-builder
- https://webflow.com/blog/color-theory
- https://www.crazyegg.com/blog/best-colors-for-pages/
- https://unbounce.com/landing-pages/elements-of-a-winning-landing-page/
- https://fonts.google.com/?category=Sans+Serif,Display&sort=popularity&subset=latin-ext&noto.lang=cs_Latn&preview.size=64&preview.layout=row

## embeded tinyletter Jarka:

``` html
<form style="border:1px solid #ccc;padding:3px;text-align:center;" action="https://tinyletter.com/Vesperi" method="post" target="popupwindow" onsubmit="window.open('https://tinyletter.com/Vesperi', 'popupwindow', 'scrollbars=yes,width=800,height=600');return true"><p><label for="tlemail">Enter your email address</label></p><p><input type="text" style="width:140px" name="email" id="tlemail" /></p><input type="hidden" value="1" name="embed"/><input type="submit" value="Subscribe" /><p><a href="https://tinyletter.com/" target="_blank">powered by TinyLetter</a></p></form>
```

Czcionki: 
- main -> Asul
- support -> Archivo
- logo -> Revans


rozkład strony:
1. Shop opening
2. Sign to newsletter
3. See my work (gallery)
4. Socials